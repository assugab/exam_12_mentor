import { IPlace } from "./IPlace.interface";
import { IUser } from "./IUser.interface";

export interface IImage {
    id: string;
    image: string;
    user: IUser;
    place: IPlace;
  }
  