import { IPlace } from "./IPlace.interface";
import { IUser } from "./IUser.interface";

export interface IReview {
  id: string;
  text: string;
  serviceScore: number;
  interiorScore: number;
  foodScore: number;

  user: IUser;
  place: IPlace;
}
