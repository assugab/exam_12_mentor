import { IUser } from "./IUser.interface";
import { IImage } from "./Image.interface.";


export interface IPlace {
    id: string;
    name: string;
    // description: string;
    user: IUser; 
    image: IImage[];
  }
  