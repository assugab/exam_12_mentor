import express, { Request, RequestHandler, Response, Router } from "express";
import { AppDataSource } from "../config/dataSource";
import { User } from "../entity/User.entity";
import { Place } from "../entity/Place.entity";
import { IImage } from "../interfaces/Image.interface.";
import { upload } from "../midleware/multer";
import { Image } from "../entity/Image.entity";

export class ImageController {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.router.get("/", this.getImages);
    this.router.get("/byid/:id", this.getImagesByPlaceId);
    this.router.get("/byuser/:id", this.getImagesByUserId);
    this.router.post("/", upload.single("image"), this.createImage);
    this.router.delete("/delete/:id", this.deleteImage);
  }

  public getRouter = () => {
    return this.router;
  };

  public createImage: RequestHandler = async (req, res, next) => {
    try {
      const file = req.file;

      if (!file) {
        const error = new Error("Please upload a file");
        return next(error);
      }

      const imageRepository = AppDataSource.getRepository(Image);
      const newImage = req.body;
      newImage.image = file.filename;

      const createdImage: IImage = await imageRepository.save(newImage);
      console.log("Image has been created");
      res.status(201).json(createdImage);
    } catch (error: unknown) {
      const err = error as Error;

      res.status(500).json({ error: err.message });
    }
  };

  private getImages = async (req: Request, res: Response) => {
    try {
      const imagesRepository = AppDataSource.getRepository(Image);
      const allImages = await imagesRepository.find();

      return res.status(200).json(allImages);
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private getImagesByPlaceId = async (req: Request, res: Response) => {
    try {
      const placeId = req.params["id"];
      if (!placeId) throw new Error("No such place in a gallery");
      const imagesRepository = AppDataSource.getRepository(Image);
      const place = await imagesRepository.find({
        where: {
          place: {
            id: Number(placeId),
          },
        },
      });

      return res.status(200).json({ place });
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private getImagesByUserId = async (req: Request, res: Response) => {
    try {
      const userId = req.params["id"];
      if (!userId) throw new Error("No such user in a gallery");
      const imagesRepository = AppDataSource.getRepository(Image);
      const users = await imagesRepository.find({
        where: {
          user: {
            id: Number(userId),
          },
        },
      });

      return res.status(200).json({ users });
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private deleteImage = async (req: Request, res: Response) => {
    try {
      const id = req.params["id"];
      const imageRepository = AppDataSource.getRepository(Image);
      const deletedImage = await imageRepository.delete({ id: Number(id) });
      console.log("Image has been deleted");
      const allPlaces = await imageRepository.find();
      return res.status(200).json(allPlaces);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
}
