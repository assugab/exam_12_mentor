import express, { Request, Response, Router } from "express";
import { AppDataSource } from "../config/dataSource";
import { User } from "../entity/User.entity";

      const userRepository = AppDataSource.getRepository(User);


export class UsersController {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.router.get("/", this.getUsers);
    this.router.get("/byid/:id", this.getUserById);
    this.router.post("/", this.addUser);
    this.router.delete("/delete/:id", this.deleteUser);
    this.router.put("/", this.editUser);
  }

  public getRouter = () => {
    return this.router;
  };

  private getUsers = async (req: Request, res: Response) => {
    try {
      const userRepository = AppDataSource.getRepository(User);
      const allUsers = await userRepository.find();

      return res.status(200).json(allUsers);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
  private getUserById = async (req: Request, res: Response) => {
    try {
      const id = req.params["id"];
      const userRepository = AppDataSource.getRepository(User);
      const user = await userRepository.findOneBy({ id: Number(id) });
      return res.status(200).json({ user });
    } catch (error) {
      return res.status(400).send(error);
    }
  };


  private addUser = async (req: Request, res: Response) => {
    try {
    
      const { displayName, password, role, username } = req.body;
      const newUser = userRepository.create({ displayName, password, role, username });
      await userRepository.save(newUser);
      // const allUsers = await userRepository.find();
      console.log("New user has been saved");
      return res.status(200).json(newUser);
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private deleteUser = async (req: Request, res: Response) => {
    try {
     const id = req.params["id"];
       const deletedUser = await userRepository.delete({ id: Number(id) });
        console.log("User has been deleted");
       const allUsers = await userRepository.find();
      return res.status(200).json(allUsers);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
  private editUser = async (req: Request, res: Response) => {
    try {
      const users = [
        { name: "John", age: 35 },
        { name: "Jane", age: 30 },
      ];
      return res.status(200).json(users);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
}
