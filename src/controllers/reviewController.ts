import express, { Request, RequestHandler, Response, Router } from "express";
import { AppDataSource } from "../config/dataSource";
import { User } from "../entity/User.entity";
import { Place } from "../entity/Place.entity";
import { IImage } from "../interfaces/Image.interface.";
import { upload } from "../midleware/multer";
import { Image } from "../entity/Image.entity";
import { Review } from "../entity/Review.entity";
import { IReview } from "../interfaces/IReview.interface";

export class ReviewController {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.router.get("/", this.getReviews);
    this.router.get("/byid/:id", this.getReviewsPlaceId);
    this.router.get("/byuser/:id", this.getReviewsByUserId);
    this.router.post("/", this.createReview);
    this.router.delete("/delete/:id", this.deleteReview);
  }

  public getRouter = () => {
    return this.router;
  };

  public createReview: RequestHandler = async (req, res, next) => {
    try {
      const reviewRepository = AppDataSource.getRepository(Review);
      const newRev = req.body;
      const newReview: IReview = await reviewRepository.save(newRev);
      console.log("Review has been created");
      res.status(201).json(newReview);
    } catch (error: unknown) {
      const err = error as Error;

      res.status(500).json({ error: err.message });
    }
  };

  private getReviews = async (req: Request, res: Response) => {
    try {
      const reviewsRepository = AppDataSource.getRepository(Review);
      const allReviews = await reviewsRepository.find();

      return res.status(200).json(allReviews);
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private getReviewsPlaceId = async (req: Request, res: Response) => {
    try {
      const placeId = req.params["id"];
      if (!placeId) throw new Error("No such place in a gallery");
      const revRepository = AppDataSource.getRepository(Review);
      const reviews = await revRepository.find({
        where: {
          place: {
            id: placeId,
          },
        },
      });

      return res.status(200).json({ reviews });
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private getReviewsByUserId = async (req: Request, res: Response) => {
    try {
      const userId = req.params["id"];
      if (!userId) throw new Error("This user doesn't have reviews");
      const reviewsRepository = AppDataSource.getRepository(Review);
      const reviews = await reviewsRepository.find({
        where: {
          user: {
            id: userId,
          },
        },
      });

      return res.status(200).json({ reviews });
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private deleteReview = async (req: Request, res: Response) => {
    try {
      const id = req.params["id"];
      const revRepository = AppDataSource.getRepository(Review);
      const deletedRev = await revRepository.delete({ id: id });
      console.log("Review has been deleted");
      const allPlaces = await revRepository.find();
      return res.status(200).json(allPlaces);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
}
