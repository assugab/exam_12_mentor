import express, { Request, Response, Router } from "express";
import { AppDataSource } from "../config/dataSource";
import { User } from "../entity/User.entity";
import { Place } from "../entity/Place.entity";

export class PlaceController {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.router.get("/", this.getPlaces);
    this.router.get("/byid/:id", this.getPlaceById);
    this.router.post("/", this.addPlace);
    this.router.delete("/delete/:id", this.deletePlace);
    // this.router.put("/", this.editUser);
  }

  public getRouter = () => {
    return this.router;
  };

  private getPlaces = async (req: Request, res: Response) => {
    try {
      const placeRepository = AppDataSource.getRepository(Place);
      const allPlaces = await placeRepository.find();

      return res.status(200).json(allPlaces);
    } catch (error) {
      return res.status(400).send(error);
    }
  };
  private getPlaceById = async (req: Request, res: Response) => {
    try {
      const id = req.params["id"];
      const placeRepository = AppDataSource.getRepository(Place);
      const place = await placeRepository.findOneBy({ id: id });
      return res.status(200).json({ place });
    } catch (error) {
      return res.status(400).send(error);
    }
  };

  private addPlace = async (req: Request, res: Response) => {
    try {
      const placeRepository = AppDataSource.getRepository(Place);
      const { name, userId } = req.body;
      const newPlace = new Place();
      newPlace.name = name;
      newPlace.user = userId;

      await placeRepository.save(newPlace);
      const allPlaces = await placeRepository.find();
      console.log(`User ${userId} added new place ${name}`);
      return res.status(200).json(allPlaces);
    } catch (error) {
      return res.status(400).send(error);
    }
  };

    private deletePlace = async (req: Request, res: Response) => {
      try {
        const id = req.params["id"];
        const placeRepository = AppDataSource.getRepository(Place);
        const deletePlace = await placeRepository.delete({ id: id });
        console.log("Place has been deleted");
        const allPlaces = await placeRepository.find();
        return res.status(200).json(allPlaces);
      } catch (error) {
        return res.status(400).send(error);
      }
    };
  //   private editUser = async (req: Request, res: Response) => {
  //     try {
  //       const users = [
  //         { name: "John", age: 35 },
  //         { name: "Jane", age: 30 },
  //       ];
  //       return res.status(200).json(users);
  //     } catch (error) {
  //       return res.status(400).send(error);
  //     }
  //   };
}
