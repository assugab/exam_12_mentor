import { DataSource } from "typeorm";
import { User } from "../entity/User.entity";
import { Place } from "../entity/Place.entity";
import { Image } from "../entity/Image.entity";
import { Review } from "../entity/Review.entity";


export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "1234",
  database: "gallery",
  synchronize: true,
  logging: true,
  entities: [User, Place, Image, Review],
  subscribers: [],
  migrations: [],
});

