import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
} from "typeorm";
import { User } from "./User.entity";
import { Place } from "./Place.entity";


@Entity("reviews")
export class Review {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  text!: string;

  @Column()
  foodScore!: number;

  @Column()
  serviceScore!: number;

  @Column()
  interiorScore!: number;

  @ManyToOne(() => User, (user) => user.id)
  user!: User;

  @ManyToOne(() => Place, (place) => place.id)
  place!: Place;
}