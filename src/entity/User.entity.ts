import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  BeforeInsert,
  Unique,
} from "typeorm";
import { Place } from "./Place.entity";
import { Image } from "./Image.entity";
import { Review } from "./Review.entity";
import { IsNotEmpty, IsString } from "class-validator";
import bcrypt from "bcrypt";


@Entity("users")
@Unique("my_unique_constraint", ["username"])
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ unique: true })
  username!: string;

  @Column({ nullable: true })
  displayName!: string;

  @Column({ default: "user" })
  role!: "user" | "admin";

  @Column()
  @IsString()
  @IsNotEmpty()
  password!: string;

  @OneToMany(() => Place, (place) => place.id)
  places?: Place[];

  @OneToMany(() => Image, (image) => image.id)
  images?: Image[];

  @OneToMany(() => Review, (review) => review.id)
  reviews?: Review[];

  async comparePassword(password: string): Promise<boolean> {
    console.log(this.password);
    console.log(password);
    return await bcrypt.compare(password, this.password);
  }

  // async generateToken() {
  //   const crypto = require("crypto");
  //   this.token = crypto.randomUUID();
  // }

  @BeforeInsert()
  async hashPassword() {
    if (this.password) {
      const hashedPassword = await bcrypt.hashSync(
        this.password,
        Number(process.env.SALT_WORK_FACTOR)
      );
      this.password = hashedPassword;
    }
  }
}
