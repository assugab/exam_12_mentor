import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from "typeorm";
import { User } from "./User.entity";
import { Image } from "./Image.entity";
import { Review } from "./Review.entity";


@Entity("places")
export class Place {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @ManyToOne(() => User, (user) => user.id)
  user!: User;

  @OneToMany(() => Image, (image) => image.id)
  image!: Image[];

  @OneToMany(() => Review, (review) => review.id)
  review?: Review[];
}