import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
} from "typeorm";
import { User } from "./User.entity";
import { Place } from "./Place.entity";


@Entity("images")
export class Image {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  image!: string;

  @ManyToOne(() => User, (user) => user.id)
  user!: User;

  @ManyToOne(() => Place, (place) => place.id)
  place!: Place;
}