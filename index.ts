import express, { Express } from "express";
import dotenv from "dotenv";
import cors from "cors";
import { UsersController } from "./src/controllers/usersControllers";
import { PlaceController } from "./src/controllers/placeController";
import { ImageController } from "./src/controllers/imageController";
import { ReviewController } from "./src/controllers/reviewController";
import { AppDataSource } from "./src/config/dataSource";

dotenv.config();

class App {
  private app: Express;
  constructor() {
    this.app = express();
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(express.static("public"));
    this.app.use(express.json());
    this.app.use(cors());
  }

  public init = async () => {
    try {
      AppDataSource.initialize()
        .then(async () => {
          console.log("Connected to database...");
        })
        .catch((error) => console.log(error));

      this.app.listen(process.env.PORT, () => {
        console.log(`server is started on port ${process.env.PORT}`);
      });
      this.app.use("/users", new UsersController().getRouter());
      this.app.use("/places", new PlaceController().getRouter());
      this.app.use("/images", new ImageController().getRouter());
      this.app.use("/reviews", new ReviewController().getRouter());



    } catch (error) {
      const err = error as Error;
      console.log(err.message);
    }
  };
}

const app = new App();

app
  .init()
  .then(() => {
    console.log("server status ok");
  })
  .catch(() => {
    console.log("server status not ok");
  });
